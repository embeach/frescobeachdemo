# Contributing

Regarding the Framework: Consider to constribute to the original project: https://github.com/aicis/fresco.

If you would like to get involved in FrescoBeachDemo or corresponding projects, please let me know (emb_gitl+ba(At)mailbox.org). Or you may try to contribute directly.
