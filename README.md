FrescoBeachDemo
==============================
This repository contains a slightly modified version (fork) of the FRESCO framework (see below) were the major change is the addition of a new demonstrator project aka *BeachDemo*, which is an experimental calendar scheduling prototype (in the context of beach-volleyball) that was developed as part of the bachelor-thesis https://embeach.gitlab.io/2019thesis/2019BaThesis.pdf by embeach (published Aug 2019 in German).
 
It is supposed to be a first proof of concept as starting point to develop more complex social network applications. In the current state the performance is still to slow for any practical use and every user need a personal server to participate. In possible further experiments the architecture from https://github.com/aicis/fresco-outsourcing will be used to improve performance and to remove the need for personal servers.

The relevant files to run the demo are contained in demos/beach.
The code is licensed under MIT license.
For Questions or whatever feel free to contact me: emb_gitl+ba(At)mailbox.org

To run the demo, try the following example configuration:

* cd demos/beach
* make build
* make move2
* make run2dummy

(for more information see demos/beach/Readme.md)

The following is content from the original FRESCO Project (https://github.com/aicis/fresco):

FRESCO is a *FRamework for Efficient and Secure COmputation*, written
in Java and licensed under the open source MIT license.

The FRESCO documentation is available at
[http://fresco.readthedocs.org](http://fresco.readthedocs.org/).
