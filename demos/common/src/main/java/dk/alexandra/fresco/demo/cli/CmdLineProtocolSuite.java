package dk.alexandra.fresco.demo.cli;

import dk.alexandra.fresco.framework.DRes;
import dk.alexandra.fresco.framework.builder.numeric.DefaultPreprocessedValues;
import dk.alexandra.fresco.framework.builder.numeric.ProtocolBuilderNumeric;
import dk.alexandra.fresco.framework.builder.numeric.field.BigIntegerFieldDefinition;
import dk.alexandra.fresco.framework.builder.numeric.field.FieldElement;
import dk.alexandra.fresco.framework.configuration.NetworkConfiguration;
import dk.alexandra.fresco.framework.network.CloseableNetwork;
import dk.alexandra.fresco.framework.network.Network;
import dk.alexandra.fresco.framework.sce.evaluator.BatchedProtocolEvaluator;
import dk.alexandra.fresco.framework.sce.evaluator.BatchedStrategy;
import dk.alexandra.fresco.framework.sce.resources.ResourcePool;
import dk.alexandra.fresco.framework.sce.resources.ResourcePoolImpl;
import dk.alexandra.fresco.framework.sce.resources.storage.FilebasedStreamedStorageImpl;
import dk.alexandra.fresco.framework.sce.resources.storage.InMemoryStorage;
import dk.alexandra.fresco.framework.util.AesCtrDrbg;
import dk.alexandra.fresco.framework.util.AesCtrDrbgFactory;
import dk.alexandra.fresco.framework.util.Drbg;
import dk.alexandra.fresco.framework.util.ModulusFinder;
import dk.alexandra.fresco.framework.util.OpenedValueStoreImpl;
import dk.alexandra.fresco.framework.value.SInt;
import dk.alexandra.fresco.lib.field.integer.BasicNumericContext;
import dk.alexandra.fresco.lib.real.RealNumericContext;
import dk.alexandra.fresco.suite.ProtocolSuite;
import dk.alexandra.fresco.suite.dummy.arithmetic.DummyArithmeticProtocolSuite;
import dk.alexandra.fresco.suite.dummy.arithmetic.DummyArithmeticResourcePoolImpl;
import dk.alexandra.fresco.suite.dummy.bool.DummyBooleanProtocolSuite;
import dk.alexandra.fresco.suite.spdz.SpdzProtocolSuite;
import dk.alexandra.fresco.suite.spdz.SpdzBuilder;
import dk.alexandra.fresco.suite.spdz.SpdzResourcePool;
import dk.alexandra.fresco.suite.spdz.SpdzResourcePoolImpl;
import dk.alexandra.fresco.suite.spdz.configuration.PreprocessingStrategy;
import dk.alexandra.fresco.suite.spdz.datatypes.SpdzSInt;
import dk.alexandra.fresco.suite.spdz.storage.SpdzDataSupplier;
import dk.alexandra.fresco.suite.spdz.storage.SpdzDummyDataSupplier;
import dk.alexandra.fresco.suite.spdz.storage.SpdzMascotDataSupplier;
import dk.alexandra.fresco.suite.spdz.storage.SpdzOpenedValueStoreImpl;
import dk.alexandra.fresco.suite.spdz.storage.SpdzStorageDataSupplier;
import dk.alexandra.fresco.suite.tinytables.online.TinyTablesProtocolSuite;
import dk.alexandra.fresco.suite.tinytables.ot.TinyTablesNaorPinkasOt;
import dk.alexandra.fresco.suite.tinytables.ot.TinyTablesOt;
import dk.alexandra.fresco.suite.tinytables.prepro.TinyTablesPreproProtocolSuite;
import dk.alexandra.fresco.suite.tinytables.prepro.TinyTablesPreproResourcePool;
import dk.alexandra.fresco.suite.tinytables.util.Util;
import dk.alexandra.fresco.tools.ot.base.DhParameters;
import dk.alexandra.fresco.tools.ot.base.DummyOt;
import dk.alexandra.fresco.tools.ot.base.Ot;
import dk.alexandra.fresco.tools.ot.otextension.RotList;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Utility for reading all configuration from command line. <p> A set of default configurations are
 * used when parameters are not specified at runtime. </p>
 */
public class CmdLineProtocolSuite {

  private final int myId;
  private final int noOfPlayers;
  private final ProtocolSuite<?, ?> protocolSuite;
  private final ResourcePool resourcePool;
  private int modBitLength;
  private int maxBitLength;
  private int fixedPointPrecision;

  static String getSupportedProtocolSuites() {
    String[] strings = {"dummybool", "dummyarithmetic", "spdz", "tinytables", "tinytablesprepro"};
    return Arrays.toString(strings);
  }

  CmdLineProtocolSuite(String protocolSuiteName, Properties properties, int myId,
      int noOfParties, Supplier<Network> network, NetworkConfiguration networkConfiguration) {
    this.myId = myId;
    this.noOfPlayers = noOfParties;
    if (protocolSuiteName.equals("dummybool")) {
      this.protocolSuite = new DummyBooleanProtocolSuite();
      this.resourcePool =
          new ResourcePoolImpl(myId, noOfPlayers);
    } else if (protocolSuiteName.equals("dummyarithmetic")) {
      this.protocolSuite = dummyArithmeticFromCmdLine(properties);
      String mod = properties.getProperty("modulus",
          "67039039649712985497870124991238141152738485774711365274259660130265015367064643"
              + "54255445443244279389455058889493431223951165286470575994074291745908195329");
      this.resourcePool =
          new DummyArithmeticResourcePoolImpl(myId, noOfPlayers,
              new BigIntegerFieldDefinition(mod));
    } else if (protocolSuiteName.equals("spdz")) {
      this.protocolSuite = getSpdzProtocolSuite(properties);
      this.resourcePool =
          createSpdzResourcePool(properties,networkConfiguration.noOfParties(),networkConfiguration);
    } else if (protocolSuiteName.equals("tinytablesprepro")) {
      String tinytablesFileOption = "tinytables.file";
      String tinyTablesFilePath = properties.getProperty(tinytablesFileOption, "tinytables");
      this.protocolSuite = tinyTablesPreProFromCmdLine(properties);
      Drbg random = new AesCtrDrbg();
      TinyTablesOt baseOt = new TinyTablesNaorPinkasOt(Util.otherPlayerId(myId), random,
          DhParameters
              .getStaticDhParams());
      this.resourcePool = new TinyTablesPreproResourcePool(myId, baseOt,
          random, 128, 40, 16000, new File(
          tinyTablesFilePath), network);
    } else {
      this.protocolSuite = tinyTablesFromCmdLine(properties);
      this.resourcePool = new ResourcePoolImpl(myId, noOfPlayers);
    }
  }

  public ResourcePool getResourcePool() {
    return resourcePool;
  }

  public ProtocolSuite<?, ?> getProtocolSuite() {
    return this.protocolSuite;
  }

  private ProtocolSuite<?, ?> dummyArithmeticFromCmdLine(Properties properties) {
    String mod = properties.getProperty("modulus",
        "67039039649712985497870124991238141152738485774711365274259660130265015367064643"
            + "54255445443244279389455058889493431223951165286470575994074291745908195329");
    int maxBitLength = Integer.parseInt(properties.getProperty("maxbitlength", "150"));
    int fixedPointPrecision = Integer.parseInt(properties.getProperty("fixedPointPrecision", "16"));
    return new DummyArithmeticProtocolSuite(new BigIntegerFieldDefinition(mod), maxBitLength,
        fixedPointPrecision);
  }

  private ProtocolSuite<?, ?> getSpdzProtocolSuite(Properties properties) {
    Properties p = getProperties(properties);
    // TODO: Figure out a meaningful default for the below
    final int maxBitLength = Integer.parseInt(p.getProperty("spdz.maxBitLength", "64"));
    if (maxBitLength < 2) {
      throw new RuntimeException("spdz.maxBitLength must be > 1");
    }
    return new SpdzProtocolSuite(maxBitLength);
  }

  private Properties getProperties(Properties properties) {
    return properties;
  }

  private SpdzResourcePool createSpdzResourcePool(Properties properties, int noOfParties, NetworkConfiguration networkConfiguration)/*, , NetManager otGenerator,
      NetManager tripleGenerator,
      NetManager expPipeGenerator) */{
    
    String strat = properties.getProperty("spdz.preprocessingStrategy");
    final PreprocessingStrategy strategy = PreprocessingStrategy.valueOf(strat);
    SpdzDataSupplier supplier = null;
    if (strategy == PreprocessingStrategy.DUMMY) {
      BigInteger modulus = ModulusFinder.findSuitableModulus(512);
      supplier = new SpdzDummyDataSupplier(myId, noOfPlayers,
          new BigIntegerFieldDefinition(modulus), modulus);
    }
    if (strategy == PreprocessingStrategy.STATIC) {
      int noOfThreadsUsed = 1;
      String storageName =
          SpdzStorageDataSupplier.STORAGE_NAME_PREFIX + noOfThreadsUsed + "_" + myId + "_" + 0
              + "_";
      supplier = new SpdzStorageDataSupplier(
          new FilebasedStreamedStorageImpl(new InMemoryStorage()), storageName, noOfPlayers);
    }
    if (strategy == PreprocessingStrategy.MASCOT) {
      final int PRG_SEED_LENGTH = 256;
      this.modBitLength=Integer.parseInt(properties.getProperty("spdz.modBitLength", "64"));//TODO verify meaningful default
      this.maxBitLength=Integer.parseInt(properties.getProperty("spdz.maxBitLength", "64"));//TODO verify meaningful default
      if (modBitLength < 2) {
        throw new RuntimeException("spdz.modBitLength must be > 1");
      }
      this.fixedPointPrecision= Integer.parseInt(properties.getProperty("fixedPointPrecision", "16"));//TODO verify meaningful default
      List<Integer> partyIds =
          IntStream.range(1, noOfParties + 1).boxed().collect(Collectors.toList());
      List<Integer> ports = new ArrayList<>(noOfParties);
      for (int i = 1; i <= noOfParties; i++) {
        ports.add(networkConfiguration.getParty(i).getPort());//TODO currently only port supported (host needed)
      }
      NetManager tripleGenerator = new NetManager(ports);
      NetManager otGenerator = new NetManager(ports);
      NetManager expPipeGenerator = new NetManager(ports);
      
      Drbg drbg = getDrbg(myId, PRG_SEED_LENGTH);
      BigInteger modulus = ModulusFinder.findSuitableModulus(modBitLength);
      final BigIntegerFieldDefinition definition = new BigIntegerFieldDefinition(modulus);
      Map<Integer, RotList> seedOts =
          getSeedOts(myId, partyIds, PRG_SEED_LENGTH, drbg, otGenerator.createExtraNetwork(myId));
      FieldElement ssk = SpdzMascotDataSupplier.createRandomSsk(definition, PRG_SEED_LENGTH);
      supplier = SpdzMascotDataSupplier.createSimpleSupplier(myId, noOfParties,
          () -> tripleGenerator.createExtraNetwork(myId), modBitLength,
          definition,
          new Function<Integer, SpdzSInt[]>() {

            private SpdzMascotDataSupplier tripleSupplier;
            private CloseableNetwork pipeNetwork;

            @Override
            public SpdzSInt[] apply(Integer pipeLength) {
              if (pipeNetwork == null) {
                pipeNetwork = expPipeGenerator.createExtraNetwork(myId);
                tripleSupplier = SpdzMascotDataSupplier.createSimpleSupplier(myId, noOfParties,
                    () -> pipeNetwork, modBitLength, definition, null,
                    seedOts, drbg, ssk);
              }
              System.out.println("Warning:overwrite pipeLength to 129");
              pipeLength=129;
              DRes<List<DRes<SInt>>> pipe =
                  createPipe(myId, noOfParties, pipeLength, pipeNetwork, tripleSupplier);
              return computeSInts(pipe);
            }
          }, seedOts, drbg, ssk);
      }
    return new SpdzResourcePoolImpl(myId, noOfPlayers, new SpdzOpenedValueStoreImpl(), supplier,
        AesCtrDrbg::new);
  }
  private Drbg getDrbg(int myId, int prgSeedLength) {
    byte[] seed = new byte[prgSeedLength / 8];
    new Random(myId).nextBytes(seed);
    return AesCtrDrbgFactory.fromDerivedSeed(seed);
  }
  private Map<Integer, RotList> getSeedOts(int myId, List<Integer> partyIds, int prgSeedLength,
      Drbg drbg, Network network) {
    Map<Integer, RotList> seedOts = new HashMap<>();
    for (Integer otherId : partyIds) {
      if (myId != otherId) {
        Ot ot = new DummyOt(otherId, network);
        RotList currentSeedOts = new RotList(drbg, prgSeedLength);
        if (myId < otherId) {
          currentSeedOts.send(ot);
          currentSeedOts.receive(ot);
        } else {
          currentSeedOts.receive(ot);
          currentSeedOts.send(ot);
        }
        seedOts.put(otherId, currentSeedOts);
      }
    }
    return seedOts;
  }
  private DRes<List<DRes<SInt>>> createPipe(int myId, int noOfPlayers, int pipeLength,
      CloseableNetwork pipeNetwork, SpdzMascotDataSupplier tripleSupplier) {

    ProtocolBuilderNumeric sequential = new SpdzBuilder(
        new BasicNumericContext(maxBitLength, myId, noOfPlayers,
            tripleSupplier.getFieldDefinition()),
        new RealNumericContext(fixedPointPrecision)).createSequential();
    SpdzResourcePoolImpl tripleResourcePool =
        new SpdzResourcePoolImpl(myId, noOfPlayers, new OpenedValueStoreImpl<>(), tripleSupplier,
            AesCtrDrbg::new);

    DRes<List<DRes<SInt>>> exponentiationPipe =
        new DefaultPreprocessedValues(sequential).getExponentiationPipe(pipeLength);
    evaluate(sequential, tripleResourcePool, pipeNetwork);
    return exponentiationPipe;
  }
  private SpdzSInt[] computeSInts(DRes<List<DRes<SInt>>> pipe) {
    List<DRes<SInt>> out = pipe.out();
    SpdzSInt[] result = new SpdzSInt[out.size()];
    for (int i = 0; i < out.size(); i++) {
      DRes<SInt> sIntResult = out.get(i);
      result[i] = (SpdzSInt) sIntResult.out();
    }
    return result;
  }

  private ProtocolSuite<?, ?> tinyTablesPreProFromCmdLine(Properties properties) {
    return new TinyTablesPreproProtocolSuite();
  }

  private ProtocolSuite<?, ?> tinyTablesFromCmdLine(Properties properties) {
    String tinytablesFileOption = "tinytables.file";
    String tinyTablesFilePath = properties.getProperty(tinytablesFileOption, "tinytables");
    return new TinyTablesProtocolSuite(myId, new File(tinyTablesFilePath));
  }
  private void evaluate(ProtocolBuilderNumeric spdzBuilder, SpdzResourcePool tripleResourcePool,
      Network network) {
    BatchedStrategy<SpdzResourcePool> batchedStrategy = new BatchedStrategy<>();
    SpdzProtocolSuite spdzProtocolSuite = createProtocolSuite(maxBitLength);
    BatchedProtocolEvaluator<SpdzResourcePool> batchedProtocolEvaluator =
        new BatchedProtocolEvaluator<>(batchedStrategy, spdzProtocolSuite);
    batchedProtocolEvaluator.eval(spdzBuilder.build(), tripleResourcePool, network);
  }
  protected SpdzProtocolSuite createProtocolSuite(int maxBitLength) {
    return new SpdzProtocolSuite(maxBitLength);
  }
}
