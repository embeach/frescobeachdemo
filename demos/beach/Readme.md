
BeachDemo
==============================

This demonstrator is used to schedule beach-volleyball appointments among the participants and will compute two parts (for more detail see thesis-link in README.md): 
1. a score for every 15min time slot of a given date, that depends on availability and individual preferences.  
2. more detail information to be able to fix the appointment via external communication. 

To run the demo, try the following example configuration:

* make build
* make move2
* make run2dummy

This will build the jar, setup two local servers (one folder per server/player) and start a secure-multiparty-process (in dummy mode) using the spdz protocol with the servers as participants and show the result of the second server. Results of the first server can be found in server1/log.txt
To run the configuration in secure mode (Mascot preprocessing) replace make run2dummy with run2mascot.
To run with more players (up to 6) replace the number.
Results are documented in the corresponding thesis.
(Only the initiator-party (see cmdl help) is supposed to get results. Therefore, other player results will contain only null-values.)
