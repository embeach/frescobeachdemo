package dk.alexandra.fresco.demo;

import dk.alexandra.fresco.demo.cli.CmdLineUtil;
import dk.alexandra.fresco.framework.Application;
import dk.alexandra.fresco.framework.DRes;
import dk.alexandra.fresco.framework.builder.numeric.Collections;
import dk.alexandra.fresco.framework.builder.numeric.Numeric;
import dk.alexandra.fresco.framework.builder.numeric.ProtocolBuilderNumeric;
import dk.alexandra.fresco.framework.configuration.NetworkConfiguration;
import dk.alexandra.fresco.framework.sce.SecureComputationEngine;
import dk.alexandra.fresco.framework.sce.SecureComputationEngineImpl;
import dk.alexandra.fresco.framework.sce.resources.ResourcePoolImpl;
import dk.alexandra.fresco.framework.value.SInt;
import dk.alexandra.fresco.lib.collections.Matrix;
import dk.alexandra.fresco.lib.collections.MatrixUtils;
import dk.alexandra.fresco.suite.ProtocolSuite;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;

import com.google.gson.Gson;

/**
 * beach demo
 * demo to show how a calendar scheduling application could be build.
 * the application is documented in the bachelor-thesis aug 2019 at th-lübeck, germany
 * License: MIT https://opensource.org/licenses/MIT
 * @author embeach (emb_gitl+ba@mailbox.org)
 *
 */
public class BeachDemo implements Application<BeachDemo.BeachResult, ProtocolBuilderNumeric> {
  /**
   * json input file see JsonInput
   */
  private final JsonInput jsonInput;
  /**
   * icomId (relates to hostfile)
   */
  private final int id;
  /**
   * number of participants
   */
  private static int nofParties = 0;
  /**
   * extended nofParties to allow shuffling
   * constraint: nofParties<=nofPartiesExtended==2^i with i is natural number
   */
  private static int nofPartiesExtended=2;//pow of 2
  /**
   * index of the date within Json file
   */
  private static int dateIndex=0;
  /**
   * date of the availability-request
   */
  private static String date;//"2019-05-05";
  /**
   * icomId of the initiator
   */
  private static int initiatorId=1;

  /**
   * Construct a new BeachDemo.
   *
   * @param id
   *          the party id (icomId)
   * @param jsonInput
   *          the json input data
   */
  public BeachDemo(int id, JsonInput jsonInput) {
    this.id = id;
    this.jsonInput = jsonInput;
  }

  /**
   * The main method sets up application specific command line parameters, parses command line
   * arguments. Based on the command line arguments it configures the SCE, instantiates the
   * BeachDemo and runs the BeachDemo on the SCE.
   * the results are only provided to the initiator party. (the other get null results)
   * @throws IOException 
   * @throws Exception 
   */
  public static <T extends ResourcePoolImpl> void main(String[] args) throws IOException {
    CmdLineUtil<T, ProtocolBuilderNumeric> util = new CmdLineUtil<>();

    util.addOption(Option.builder("jf")
        .desc("Json file containing necessary beachApp related inputs")
        .longOpt("jsonFile").hasArg().build());
    util.addOption(Option.builder("hf").desc(
        "File containing a player id and networkconfiguration in each line (i.e. 1:localhost:8081)")
        .longOpt("hostsFile").hasArg().build());
    util.addOption(Option.builder("iId").desc(
        "Initiator icomId (internal communication id). The initiator is the requesting party (icomId are the ids from the hostFile)")
        .longOpt("initiatorIcomId").hasArg().build());
    util.addOption(Option.builder("d").desc(
        "Date for the availability-request (i.e. 2019-05-05)")
        .longOpt("date").hasArg().build());
    CommandLine cmd = util.parse(args);
    cmd = util.parse(args);
    NetworkConfiguration networkConfiguration = util.getNetworkConfiguration();
    nofParties = networkConfiguration.noOfParties();
    while(nofPartiesExtended<nofParties) {//needed for the shuffle method: //calculate size that holds nofParties<=size and size==2^i with i is natural number
      nofPartiesExtended*=2;
    }
    //some validation
    if (!cmd.hasOption("jf")) {
      throw new IllegalArgumentException("Player  must submit inputs (JsonFile)");
    }
    if (!cmd.hasOption("d")) {
      throw new IllegalArgumentException("Player  must submit inputs (date)");
    }
    if (!cmd.hasOption("iId")) {
      throw new IllegalArgumentException("Player  must submit inputs (initiatorIcomId)");
    }
    date=cmd.getOptionValue("d");
    initiatorId=Integer.valueOf(cmd.getOptionValue("iId"));
    
    //print options
    System.out.println("main was called with the following options:");
    for (Option o : cmd.getOptions()) {
      System.out.println("option: " + o.getOpt() + " : " + o.getValue());
    }
    
    // Parse jsonInput
    String jsonFile= cmd.getOptionValue("jf");
    String json=Files.readAllLines(Paths.get(jsonFile)).stream().collect(Collectors.joining("\n"));
    Gson gson=new Gson();
    JsonInput jsonInput = gson.fromJson(json, JsonInput.class);
    jsonInput.processInput(nofParties);
    dateIndex=findDateIndex(jsonInput);
    // print input
    printInput(jsonInput);

    // Do the secure computation using config from property files.
    System.out.println("going to create new BeachDemo(..)");
    assert networkConfiguration.getMyId()==jsonInput.knownPlayersMapLocalId.get(jsonInput.ownPlayerLocalId).icomId;
    BeachDemo privateBeachDemo = new BeachDemo(networkConfiguration.getMyId(), jsonInput);
    System.out.println("going to getProtocolSuite()");
    ProtocolSuite<T, ProtocolBuilderNumeric> psConf = util.getProtocolSuite();
    System.out.println("going to create new SecureComputationEngineImpl");
    SecureComputationEngine<T, ProtocolBuilderNumeric> sce = new SecureComputationEngineImpl<>(
        psConf, util.getEvaluator());
    T resourcePool = util.getResourcePool();
    System.out.println("going to runApplication(..)");
    BeachResult beachResult = sce.runApplication(privateBeachDemo, resourcePool, util.getNetwork(),Duration.ofHours(8)/*.ofMinutes(90)*/);
    util.closeNetwork();
    
    // Print result.
    System.out.println("Results for date "+cmd.getOptionValue("d")+" and special beach-place1:");//place is hardcoded!
    System.out.println("Number of smpc-parties:"+nofParties);
    System.out.println("The resulting (availability sum | maybe sum | score ) are:");
    for (int j = 0; j < CalendarAndComIdsEntry.LIST_LENGTH; j++) {
      System.out.println("time-index(" + j + "): " + beachResult.outs1.get(j)+" | "+beachResult.outs3.get(j)+" | "+beachResult.outs2.get(j));
    }
    for(int i=0;i<CalendarAndComIdsEntry.LIST_LENGTH;i++) {
      Matrix<BigInteger> detailsOpenOut=beachResult.oneDateDetailsMsOpenOut.get(i);
      System.out.println("Details time-index("+i+") (availability|comId|localId):");
      for(int j=0;j<nofPartiesExtended;j++) { 
        for(int k=0;k<3;k++) {
          System.out.print("|"+detailsOpenOut.getRow(j).get(k));
        }
        System.out.print("\n");
      }
    }
  }
  /**
   * prints some data from json input
   * @param jsonInput
   */
  public static void printInput(JsonInput jsonInput) {
    System.out.println("jsonInput.calendarAndComIds.get(0).as:"+jsonInput.calendarAndComIds.get(0).as);
    System.out.println("known players:");
    jsonInput.knownPlayers.stream().forEachOrdered(p->{
      System.out.println("p.localId:"+p.localId);
      System.out.println("p.icomId:"+p.icomId);
      System.out.println("p.nickname:"+p.nickname);
      System.out.println("p.getWeight(..):"+p.getWeight(jsonInput.weights,jsonInput.defaultWeight));
    });
    System.out.println("jsonInput.ownPlayerLocalId:"+jsonInput.ownPlayerLocalId);
    System.out.println("jsonInput.weights:");
    for(WeightEntry w:jsonInput.weights) {
      System.out.println("playerLocalId:"+w.playerLocalId);
      System.out.println("weight:"+w.weight);
    }
    System.out.println("jsonInput.defaultWeight:"+jsonInput.defaultWeight);
    System.out.println("jsonInput.knownPlayersExtWeights:"+jsonInput.knownPlayersExtWeights);
  }
  /**
   * searches for date (given by cmd)
   * @param jsonInput
   * @return date-index within calendarAndComIds entries
   * @throws RuntimeException if date not found
   */
  public static int findDateIndex(JsonInput jsonInput){
    for(int i=0;i<jsonInput.calendarAndComIds.size();i++) {
      if(jsonInput.calendarAndComIds.get(i).date.equals(date)) {
        return i;
      }
    }
    throw new RuntimeException("date not found");
  }
  /**
   * builds the specific mpc-computation with input data and  standard lib
   */
  @Override
  public DRes<BeachResult> buildComputation(ProtocolBuilderNumeric producer) {
    return producer.seq(seq -> {
      Collections collections = seq.collections();
      Numeric num=seq.numeric();
      BeachData beachData=new BeachData();
      
      // Handle input lists
      beachData.oneDateAMsOverPlayers = new ArrayList<>(nofParties);
      beachData.oneDateAsOverPlayers = new ArrayList<>(nofParties);
      beachData.oneDateMsOverPlayers = new ArrayList<>(nofParties);
      beachData.oneDateComIdsOverPlayers = new ArrayList<>(nofParties);
      CalendarAndComIdsEntry calAndCom=jsonInput.calendarAndComIds.get(dateIndex);
      beachData.passNumsOverPlayers= new ArrayList<>(nofParties);
      for (int id = 1; id <= nofParties; id++) {
        DRes<List<DRes<SInt>>> oneDateAMs;
        DRes<List<DRes<SInt>>> oneDateAs;
        DRes<List<DRes<SInt>>> oneDateMs;
        DRes<List<DRes<SInt>>> oneDateComIds;
        if (this.id == id) {
          oneDateAMs= collections.closeList(calAndCom.ams, id);
          oneDateAs= collections.closeList(calAndCom.as, id);
          oneDateMs= collections.closeList(calAndCom.ms, id);
          oneDateComIds= collections.closeList(calAndCom.comIds, id);
          beachData.passNumsOverPlayers.add(num.input(jsonInput.knownPlayersMapIcomId.get(this.id).passNumber,id));
        }else {
          oneDateAMs= collections.closeList(CalendarAndComIdsEntry.LIST_LENGTH, id);
          oneDateAs= collections.closeList(CalendarAndComIdsEntry.LIST_LENGTH, id);
          oneDateMs= collections.closeList(CalendarAndComIdsEntry.LIST_LENGTH, id);
          oneDateComIds= collections.closeList(CalendarAndComIdsEntry.LIST_LENGTH, id);
          beachData.passNumsOverPlayers.add(num.input(null,id));
        }
        beachData.oneDateAMsOverPlayers.add(oneDateAMs);
        beachData.oneDateAsOverPlayers.add(oneDateAs);
        beachData.oneDateMsOverPlayers.add(oneDateMs);
        beachData.oneDateComIdsOverPlayers.add(oneDateComIds);
      }
      beachData.knownPlayersExtPassNumsOverPlayers = new ArrayList<>(nofParties);
      beachData.knownPlayersExtWeightsOverPlayers = new ArrayList<>(nofParties);
      beachData.knownPlayersExtLocalIdsOverPlayers = new ArrayList<>(nofParties);
      for (int id = 1; id <= nofParties; id++) {
        DRes<List<DRes<SInt>>> knownPlayersExtPassNums;
        DRes<List<DRes<SInt>>> knownPlayersExtWeights;
        DRes<List<DRes<SInt>>> knownPlayersExtLocalIds;
        if(id==this.id) {
            knownPlayersExtPassNums=collections.closeList(jsonInput.knownPlayersExtPassNums, this.id);
            knownPlayersExtWeights=collections.closeList(jsonInput.knownPlayersExtWeights, this.id);
            knownPlayersExtLocalIds=collections.closeList(jsonInput.knownPlayersExtLocalIds, this.id);
        }else {
            knownPlayersExtPassNums=collections.closeList(nofParties, id);
            knownPlayersExtWeights=collections.closeList(nofParties, id);
            knownPlayersExtLocalIds=collections.closeList(nofParties, id);
        }
        beachData.knownPlayersExtPassNumsOverPlayers.add(knownPlayersExtPassNums);
        beachData.knownPlayersExtWeightsOverPlayers.add(knownPlayersExtWeights);
        beachData.knownPlayersExtLocalIdsOverPlayers.add(knownPlayersExtLocalIds);
      }

      return () -> {
        MatrixUtils matrixUtils=new MatrixUtils();
        beachData.oneDateAMsOverPlayersM=matrixUtils.unwrapRows(beachData.oneDateAMsOverPlayers);
        beachData.oneDateAsOverPlayersM=matrixUtils.unwrapRows(beachData.oneDateAsOverPlayers);
        beachData.oneDateMsOverPlayersM=matrixUtils.unwrapRows(beachData.oneDateMsOverPlayers);
        beachData.oneDateComIdsOverPlayersM=matrixUtils.unwrapRows(beachData.oneDateComIdsOverPlayers);
        beachData.knownPlayersExtPassNumsOverPlayersM=matrixUtils.unwrapRows(beachData.knownPlayersExtPassNumsOverPlayers);
        beachData.knownPlayersExtWeightsOverPlayersM=matrixUtils.unwrapRows(beachData.knownPlayersExtWeightsOverPlayers);
        beachData.knownPlayersExtLocalIdsOverPlayersM=matrixUtils.unwrapRows(beachData.knownPlayersExtLocalIdsOverPlayers);
        beachData.initiatorKnownPlayersExtPassNumsOut=beachData.knownPlayersExtPassNumsOverPlayersM.getRow(initiatorId-1);//initiatorKnownPlayersExtPassNums.out();
        beachData.initiatorKnownPlayersExtWeightsOut=beachData.knownPlayersExtWeightsOverPlayersM.getRow(initiatorId-1);//initiatorKnownPlayersExtWeights.out();
        beachData.initiatorKnownPlayersExtLocalIdsOut=beachData.knownPlayersExtLocalIdsOverPlayersM.getRow(initiatorId-1);
        return beachData;
      };
    }).seq((seq, beachData) -> {
      Numeric num=seq.numeric();
      //create beachData.initiatorKnownPlayersExtLocalIdsOutReduced
      beachData.initiatorKnownPlayersExtLocalIdsOutReduced=new ArrayList<>();
      for(int i=0;i<nofParties;i++) {
        //seq.debug().openAndPrint("beachData.initiatorKnownPlayersExtPassNumsOut.get(i)",beachData.initiatorKnownPlayersExtPassNumsOut.get(i),System.out);
        //seq.debug().openAndPrint("beachData.passNumsOverPlayers.get(i)",beachData.passNumsOverPlayers.get(i),System.out);
        DRes<SInt> isEqual=seq.comparison().equals(32,beachData.initiatorKnownPlayersExtPassNumsOut.get(i),beachData.passNumsOverPlayers.get(i));
        //seq.debug().openAndPrint("isEqual",isEqual,System.out);
        beachData.initiatorKnownPlayersExtLocalIdsOutReduced.add(seq.advancedNumeric().condSelect(isEqual, beachData.initiatorKnownPlayersExtLocalIdsOut.get(i), seq.numeric().known(0l)));
      }
      //create beachData.oneDateDetailsMs
      beachData.oneDateDetailsMs=new ArrayList<>(CalendarAndComIdsEntry.LIST_LENGTH);
      for(int i=0;i<CalendarAndComIdsEntry.LIST_LENGTH;i++) {
        ArrayList<ArrayList<DRes<SInt>>> details=new ArrayList<>();
        for(int j=0;j<nofPartiesExtended;j++) {
          if(j<nofParties) {
            details.add(new ArrayList<>(Arrays.asList(
              beachData.oneDateAMsOverPlayersM.getColumn(i).get(j),
              beachData.oneDateComIdsOverPlayersM.getColumn(i).get(j),
              beachData.initiatorKnownPlayersExtLocalIdsOutReduced.get(j)
              )));
          }else {
            details.add(new ArrayList<>(Arrays.asList(num.known(0l),num.known(0l),num.known(0l))));
          }
        }
        beachData.oneDateDetailsMs.add(new Matrix<>(nofPartiesExtended,3,details));
      }
      beachData.oneDateDetailsMsShuffled=new ArrayList<>();
      for(int i=0;i<CalendarAndComIdsEntry.LIST_LENGTH;i++) {
        final Integer ii=i;
        beachData.oneDateDetailsMsShuffled.add(seq.collections().shuffle(()->{
          return beachData.oneDateDetailsMs.get(ii);
        }));
      }
      return ()->beachData;
    }).seq((seq, beachData) -> {
      beachData.oneDateAsSum=new ArrayList<>();
      for (int j = 0; j < CalendarAndComIdsEntry.LIST_LENGTH; j++) {
        beachData.oneDateAsSum.add(seq.advancedNumeric().sum(beachData.oneDateAsOverPlayersM.getColumn(j)));
      }
      beachData.oneDateMsSum=new ArrayList<>();
      for (int j = 0; j < CalendarAndComIdsEntry.LIST_LENGTH; j++) {
        beachData.oneDateMsSum.add(seq.advancedNumeric().sum(beachData.oneDateMsOverPlayersM.getColumn(j)));
      }
      //create weighted availability-matrix based on oneDateAsOverPlayers and initiatorKnownPlayersExtWeightsOut TODO rename to AMs
      beachData.oneDateWeightedAsOverPlayers=new ArrayList<>(); 
      for(int i=0;i<nofParties;i++) {
        List<DRes<SInt>> row_a=beachData.oneDateAMsOverPlayersM.getRow(i);//availabilities of one player
        DRes<SInt> weight=beachData.initiatorKnownPlayersExtWeightsOut.get(i);
        ArrayList<DRes<SInt>> oneDateWeigtedAs=new ArrayList<>(); 
        //iterate over time-indexes
        for(int j=0;j<CalendarAndComIdsEntry.LIST_LENGTH;j++) {
          //oneDateWeigtedAs.add(seq.numeric().mult(seq.numeric().add(seq.numeric().mult(BigInteger.valueOf(2l),row_a.get(j)),row_m.get(j)),weight));
          oneDateWeigtedAs.add(seq.numeric().mult(row_a.get(j),weight));
        }
        beachData.oneDateWeightedAsOverPlayers.add(oneDateWeigtedAs);
      }
      beachData.oneDateWeightedAsOverPlayersM=new Matrix<>(nofParties,CalendarAndComIdsEntry.LIST_LENGTH,beachData.oneDateWeightedAsOverPlayers);
      beachData.oneDateWeightedAsSum=new ArrayList<>();
      for (int j = 0; j < CalendarAndComIdsEntry.LIST_LENGTH; j++) {
        beachData.oneDateWeightedAsSum.add(seq.advancedNumeric().sum(beachData.oneDateWeightedAsOverPlayersM.getColumn(j)));
      }
      return () -> beachData;
    }).seq((seq, beachData) -> {
      //TODO use Matrix-Utils to out and open values.
      List<SInt> res1 = beachData.oneDateAsSum.stream().map(DRes::out).collect(Collectors.toList());
      beachData.output1 = new ArrayList<>();
      for (SInt b : res1) {
        beachData.output1.add(seq.numeric().open(b,initiatorId));
      }
      List<SInt> res2 = beachData.oneDateWeightedAsSum.stream().map(DRes::out).collect(Collectors.toList());
      beachData.output2 = new ArrayList<>();
      for (SInt b : res2) {
        beachData.output2.add(seq.numeric().open(b,initiatorId));
      }
      List<SInt> res3 = beachData.oneDateMsSum.stream().map(DRes::out).collect(Collectors.toList());
      beachData.output3 = new ArrayList<>();
      for (SInt b : res3) {
        beachData.output3.add(seq.numeric().open(b,initiatorId));
      }
      beachData.oneDateDetailsMsOpen=new ArrayList<>();
      for(int i=0;i<CalendarAndComIdsEntry.LIST_LENGTH;i++) {//TODO refactor with matrix-utils
        Matrix<DRes<SInt>> detailsClosed=beachData.oneDateDetailsMsShuffled.get(i).out();
        ArrayList<ArrayList<DRes<BigInteger>>> detailsOpen=new ArrayList<>();
        for(int j=0;j<nofPartiesExtended;j++) {
          ArrayList<DRes<BigInteger>>detailsOpen_row=new ArrayList<>();
          for(int k=0;k<3;k++) {
            detailsOpen_row.add(seq.numeric().open(detailsClosed.getRow(j).get(k).out(),initiatorId));
          }
          detailsOpen.add(detailsOpen_row);
        }
        beachData.oneDateDetailsMsOpen.add(new Matrix<DRes<BigInteger>>(nofPartiesExtended,3,detailsOpen));
      }
      return () -> beachData;
    }).seq((seq, beachData) -> {
      BeachResult beachResult=new BeachResult();
      beachResult.outs1 = beachData.output1.stream().map(DRes::out).collect(Collectors.toList());
      beachResult.outs2 = beachData.output2.stream().map(DRes::out).collect(Collectors.toList());
      beachResult.outs3 = beachData.output3.stream().map(DRes::out).collect(Collectors.toList());
      beachResult.oneDateDetailsMsOpenOut=new ArrayList<>();
      for(int i=0;i<CalendarAndComIdsEntry.LIST_LENGTH;i++) {//TODO refactor with matrix-utils
        Matrix<DRes<BigInteger>> detailsOpen=beachData.oneDateDetailsMsOpen.get(i);
        ArrayList<ArrayList<BigInteger>> detailsOpenOut=new ArrayList<>();
        for(int j=0;j<nofPartiesExtended;j++) {
          ArrayList<BigInteger>detailsOpenOut_row=new ArrayList<>();
          for(int k=0;k<3;k++) {
            detailsOpenOut_row.add(detailsOpen.getRow(j).get(k).out());
          }
          detailsOpenOut.add(detailsOpenOut_row);
        }
        beachResult.oneDateDetailsMsOpenOut.add(new Matrix<BigInteger>(nofPartiesExtended,3,detailsOpenOut));
      }
      return () -> beachResult;
    });
  }
  /**
   * container class that is used to hold data during smpc-computation
   * @author user
   *
   */
  private static final class BeachData {
    //input data
    /**
     * the outer list iterates over players/parties
     * ordered by icomId
     * the inner list iterates over time-index-values (ams) at a specific date
     */
    private List<DRes<List<DRes<SInt>>>> oneDateAMsOverPlayers;
    /**
     * like oneDateAMsOverPlayers but relates to (as)
     */
    private List<DRes<List<DRes<SInt>>>> oneDateAsOverPlayers;
    /**
     * like oneDateAMsOverPlayers but relates to (ms)
     */
    private List<DRes<List<DRes<SInt>>>> oneDateMsOverPlayers;
    /**
     * like oneDateAMsOverPlayers but relates to (comIds)
     */
    private List<DRes<List<DRes<SInt>>>> oneDateComIdsOverPlayers;
    /**
     * list of passNums ordered by icomId
     */
    private List<DRes<SInt>> passNumsOverPlayers;
    /**
     * the outer list iterates over players/parties (ordered by icomId)
     * the inner list iterates over player/parties (ordered by icomId)
     * but the inner list values are the given weights in the json input
     * if no weight is given the JsonInput.defaultWeight applies
     */
    private List<DRes<List<DRes<SInt>>>> knownPlayersExtWeightsOverPlayers;
    /**
     * like knownPlayersExtWeightsOverPlayers but values are loclIds
     */
    private List<DRes<List<DRes<SInt>>>> knownPlayersExtLocalIdsOverPlayers;
    /**
     * like knownPlayersExtWeightsOverPlayers but values are passNums
     */
    private List<DRes<List<DRes<SInt>>>> knownPlayersExtPassNumsOverPlayers;
    /**
     * like oneDateAMsOverPlayers but as matrix
     */
    private Matrix<DRes<SInt>> oneDateAMsOverPlayersM;
    /**
     * like oneDateAsOverPlayers but as matrix
     */
    private Matrix<DRes<SInt>> oneDateAsOverPlayersM;
    /**
     * like oneDateMsOverPlayers but as matrix
     */
    private Matrix<DRes<SInt>> oneDateMsOverPlayersM;
    /**
     * like oneDateComIdsOverPlayers but as matrix
     */
    private Matrix<DRes<SInt>> oneDateComIdsOverPlayersM;
    /**
     * like knownPlayersExtWeightsOverPlayers but as matrix
     */
    private Matrix<DRes<SInt>>knownPlayersExtWeightsOverPlayersM;
    /**
     * like knownPlayersExtLocalIdsOverPlayers but as matrix
     */
    private Matrix<DRes<SInt>>knownPlayersExtLocalIdsOverPlayersM;
    /**
     * like knownPlayersExtPassNumsOverPlayers but as matrix
     */
    private Matrix<DRes<SInt>> knownPlayersExtPassNumsOverPlayersM;
    /**
     * list of matrixes. each matrix contains details over a specific time-slot (1/4h)
     * the height of matrix is BeachDemo.nofPartiesExtended the width is 3. the three values are: am (from ams),comId,localId
     * with this details the initiator is able to invite parties.
     * localIds are protected by passNumber
     * CalendarAndComIdsEntry.LIST_LENGTH:BeachDemo.nofPartiesExtended:3:<SInt>
     */
    private List<Matrix<DRes<SInt>>> oneDateDetailsMs;
    /**
     * list of passNums from the initiators knownPlayers. 0 if player is not included in initiators knownPlayers or passNum is not known (by initiator).
     */
    private List<DRes<SInt>> initiatorKnownPlayersExtPassNumsOut;
    /**
     * list of weights from the initiators knownPlayers. initiators default weight if player is not included in initiators knownPlayers or does not have specific weight.
     */
    private List<DRes<SInt>> initiatorKnownPlayersExtWeightsOut;
    /**
     * list of localIds from the initiators knownPlayers. 0 if player is not included in initiators knownPlayers.
     */
    private List<DRes<SInt>> initiatorKnownPlayersExtLocalIdsOut;
    //intermediate data
    /**
     * every row of oneDateAMsOverPlayersM is multiplied with the corresponding player weight from initiatorKnownPlayersExtWeightsOut
     */
    private ArrayList<ArrayList<DRes<SInt>>> oneDateWeightedAsOverPlayers;//TODO rename to AMs
    /**
     * the values from oneDateWeightedAsOverPlayers as matrix
     */
    private Matrix<DRes<SInt>>oneDateWeightedAsOverPlayersM;//TODO rename to AMs
    /**
     * detail matrices that hold information about a specific time-slot on a specific date, 
     * with height=BeachDemo.nofPartiesExtended, width=3
     * columns: availability|comId|localId
     */
    private List<Matrix<DRes<BigInteger>>> oneDateDetailsMsOpen;
    /**
     * list of all time-slots of a given date where every value is the sum of the corresponding column from oneDateAsOverPlayersM
     */
    private List<DRes<SInt>> oneDateAsSum;
    /**
     * list of all time-slots of a given date where every value is the sum of the corresponding column from oneDateMsOverPlayersM
     */
    private List<DRes<SInt>> oneDateMsSum;
    /**
     * list of all time-slots of a given date where every value is the sum of the corresponding column from oneDateWeightedAsOverPlayersM
     */
    private List<DRes<SInt>> oneDateWeightedAsSum;//TODO rename to AMs
    /**
     * shuffled version of oneDateDetailsMs where all rows are shuffled so that the player icomId can not be deduced from row number.
     */
    private List<DRes<Matrix<DRes<SInt>>>> oneDateDetailsMsShuffled;
    /**
     * reduced version of initiatorKnownPlayersExtLocalIdsOut, where only those ids are included where initiator has the correct passNum
     */
    private List<DRes<SInt>> initiatorKnownPlayersExtLocalIdsOutReduced;
    //output data
    /**
     * availability sum for every time-slot at a specific date
     */
    private List<DRes<BigInteger>> output1;//TODO rename to proper names
    /**
     * score for every time-slot at a specific date
     */
    private List<DRes<BigInteger>> output2;//TODO rename to proper names
    /**
     * "maybe sum" for every time-slot at a specific date
     */
    private List<DRes<BigInteger>> output3;//TODO rename to proper names

  }
  /**
   * container class to hold the results of the smpc-computation
   * @author user
   *
   */
  public static final class BeachResult {
    /**
     * detail matrices that hold information about a specific time-slot on a specific date, 
     * with height=BeachDemo.nofPartiesExtended, width=3
     * columns: availability|comId|localId
     */
    private List<Matrix<BigInteger>> oneDateDetailsMsOpenOut;
    /**
     * availability sum for every time-slot at a specific date
     */
    private List<BigInteger> outs1;//TODO rename to proper names
    /**
     * score for every time-slot at a specific date
     */
    private List<BigInteger> outs2;//TODO rename to proper names
    /**
     * "maybe sum" for every time-slot at a specific date
     */
    private List<BigInteger> outs3;//TODO rename to proper names
  }
  /**
   * holds the json data and prepares them for processing
   * @author user
   *
   */
  private static final class JsonInput{
    /**
     * localId of the player (self) within the knownPlayers list.
     * constrains? 1-?
     */
    int ownPlayerLocalId;
    /**
     * list of player-weight entries to influence the score of the possible meetings
     */
    List<WeightEntry> weights=new ArrayList<>();
    /**
     * see CalendarAndComIdsEntry
     */
    List<CalendarAndComIdsEntry> calendarAndComIds=new ArrayList<>();
    /**
     * see KnownPlayer
     */
    List<KnownPlayer> knownPlayers=new ArrayList<>();
    /**
     * map to access known player with there localId
     */
    transient Map<Integer,KnownPlayer> knownPlayersMapLocalId=new HashMap<>();
    /**
     * map to access known player with there icomId
     */
    transient Map<Integer,KnownPlayer> knownPlayersMapIcomId=new HashMap<>();
    /**
     * extended list of passNums from the knownPlayers. 0 if player is not included in knownPlayers or passNum is not known. length=nofParties
     */
    List<BigInteger> knownPlayersExtPassNums=new ArrayList<>();
    /**
     * extended list of localIds from the knownPlayers. 0 if player is not included in knownPlayers. length=nofParties
     */
    List<BigInteger> knownPlayersExtLocalIds=new ArrayList<>();
    /**
     * /**
     * extended list of weights from the knownPlayers. defaultWeight if player is not included in knownPlayers. length=nofParties
     */
    List<BigInteger> knownPlayersExtWeights=new ArrayList<>();
    /**
     * defaultWeight that should applied if no specific weight is given for knownPlayer or player is not included in knonwPlayers.
     */
    transient BigInteger defaultWeight=BigInteger.valueOf(1);
    /**
     * create lists etc. that are derived from input data
     * @param nofParties
     */
    public void processInput(int nofParties){
      for(CalendarAndComIdsEntry calendarAndComIdsEntry:calendarAndComIds) {
        calendarAndComIdsEntry.generate_as_ms_ams_comIds();
      }
      setDefaultWeight();
      generateKnownPlayersMaps();
      generateKnownPlayersExtendedLists(nofParties);
      //TODO validation
    }
    /**
     * create knownPlayersMapLocalId and knownPlayersMapIcomId
     */
    public void generateKnownPlayersMaps() {
      assert(knownPlayersMapLocalId.isEmpty());
      assert(knownPlayersMapIcomId.isEmpty());
      for(KnownPlayer p:knownPlayers) {
        knownPlayersMapLocalId.put(p.localId, p);
        knownPlayersMapIcomId.put(p.icomId, p);
      }
    }
    /**
     * fill up knownPlayer-value-lists to reach size=nofParties (this is needed to have equal size lists for every party)
     * @param nofParties
     */
    public void generateKnownPlayersExtendedLists(int nofParties) {
      assert(knownPlayersExtPassNums.isEmpty());
      assert(knownPlayersExtWeights.isEmpty());
      for(int i=1;i<=nofParties;i++) {
        knownPlayersExtPassNums.add((knownPlayersMapIcomId.containsKey(i))?knownPlayersMapIcomId.get(i).passNumber:BigInteger.valueOf(0));
        knownPlayersExtLocalIds.add((knownPlayersMapIcomId.containsKey(i))?BigInteger.valueOf(knownPlayersMapIcomId.get(i).localId):BigInteger.valueOf(0));
        BigInteger weight;
        if(knownPlayersMapIcomId.containsKey(i)) {
          KnownPlayer currP=knownPlayersMapIcomId.get(i);
          weight=currP.getWeight(this.weights, this.defaultWeight);
        }else {
          weight=this.defaultWeight;
        }
        knownPlayersExtWeights.add(weight);
      }
    }
    /**
     * create defaultWeight
     */
    public void setDefaultWeight() {
      BigInteger defaultWeight=BigInteger.valueOf(1);
      for(WeightEntry we:weights) {
        if(we.playerLocalId==0) {
          defaultWeight=BigInteger.valueOf(we.weight);
          break;
        }
      }
      this.defaultWeight=defaultWeight;
    }
  }
  /**
   * holds the weight value that refers to specific playerLocalId or holds defaultWeight if playerLocalId is 0
   * @author user
   *
   */
  private static final class WeightEntry{
    /**
     * unique id in knownPlayers or 0
     */
    int playerLocalId;//constrains? unique
    /**
     * weight to weight a knownPlayer to compute the score of a time-slot
     */
    int weight;//constrains? 0-?
  }
  private static final class CalendarAndComIdsEntry{
    /**
     * date for the availability-data
     * constrains? iso i.e. 2019-02-21
     */
    String date;
    /**
     * list of availability-values for one day (24h)
     * constrains? 96 (lIST_LENGTH) (for every quarter hour one) 2=probably available,1=maybe,0=probably not available
     */
    List<Integer> availabilities=new ArrayList<>();
    /**
     * list of communication ids. for every availability-value one.
     * constrains? 96 (lIST_LENGTH) 
     * (every entry is an anonymous communication channel id that the player controls (but authentication has to be proven afterwards)) 
     * channel ids kann be reused - as tradeoff between anonymity and transparency for the beach-meeting-group
     */
    List<BigInteger> comIds=new ArrayList<>();
    /**
     * like availabilities but with short name an BigInteger
     * (BigInteger seems to be needed for fresco)
     */
    transient List<BigInteger> ams=new ArrayList<>();
    /**
     * list of availability-values
     * similar to ams but with only 1 or 0 allowed
     * 1=available or maybe,0=not available 
     */
    transient List<BigInteger> as=new ArrayList<>();
    /**
     * list of maybe-values
     * similar to ams but with only 1 or 0 allowed
     * 1=maybe, 0=not available
     */
    transient List<BigInteger> ms=new ArrayList<>();
    /**
     * number of time-fractions 96*1/4 h = 24h
     */
    public static final int LIST_LENGTH=96;
    /**
     * created needed lists from raw inputs
     */
    public void generate_as_ms_ams_comIds(){
      assert(as.isEmpty());
      assert(ms.isEmpty());
      assert(availabilities.size()==96);
      for(int am:availabilities) {
        ams.add(BigInteger.valueOf(am));
        as.add((am==2||am==1)?BigInteger.valueOf(1):BigInteger.valueOf(0));
        ms.add(am==1?BigInteger.valueOf(1):BigInteger.valueOf(0));
      }
      //hide unneccessary comIds
      for(int i=0;i<LIST_LENGTH;i++) {
        if(availabilities.get(i)==0) {
          comIds.set(i, BigInteger.valueOf(0));
        }
      }
    }
  }
  /**
   * player that is known by the server owner - it means server owner can attache information to the players icomId
   * @author user
   *
   */
  private static final class KnownPlayer{
    /**
     * unique id in InputJson.knownPlayers
     */
    int localId;//constrains? unique 1-?
    /**
     * internal(means used by all smpc-participants but not externaly) communication id from the hosts file that contains server-addresses and ports of all participants. in this file every server-address gets a global id to refer.
     */
    int icomId;//constraints? unique 1-?
    /**
     * passNumber to protect the linkability of the availybilities and comIds to the icomId in the results of some initiator requesting data.
     */
    BigInteger passNumber=BigInteger.valueOf(0);
    /**
     * nickname to localy link the player to a specific person. not used elsewhere currently
     */
    String nickname="";//constrains? not null
    /**
     * provides the specified weight of the current player or defaultWeight if no weight is specified
     * @param weights of knownPlayers
     * @param defaultWeight
     * @return specified weight of the current player or defaultWeight if no weight is specified
     */
    public BigInteger getWeight(List<WeightEntry> weights,BigInteger defaultWeight){
      BigInteger weight;
      List<WeightEntry> wes=weights.stream().filter(we->we.playerLocalId==localId).collect(Collectors.toList());
      if(wes.isEmpty()) {
        weight=defaultWeight;
      }else {
        weight=BigInteger.valueOf(wes.get(0).weight);
      }
      return weight;
    }
  }

}
